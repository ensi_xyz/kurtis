Script python permettant d'interroger Kitsu pour declarer les elements dans Kurtis
et de pouvoir en ajouter.
```python
import gazu
from kurtis import api
# from kitsu
gazu.client.set_host("https://eni.cg-wire.com/api")
gazu.log_in("c.laubry@nouvellesimages.xyz", "default123")

deps = gazu.task.all_task_types()
deps_names = [d['name'] for d in deps]

collections = gazu.asset.all_asset_types()
collections_names = [d['name'] for d in collections]

project = gazu.project.get_project_by_name('Labo')
elements_dict = []
for collection in collections:
    elements = gazu.asset.all_assets_for_project_and_type(project, collection)
    for element in elements:
        elements_dict.append({'name': element['name'], 'type': collection['name']})



# for kurtis
elements = elements_dict
collections = collections_names
deps = deps_names
tasks = ['work']

# creation du block lib
root = api.getRootBlock()
lib = api.findBlock('library')
if not lib.isValid():
   lib = api.addLibrary('library', parent=root)
   path = api.Var('path', 'block_name + "/"', type='+')
   lib.addVar(path)

# creations des collections
for collection in collections:
    coll = api.findBlock(collection)
    if not coll.isValid():
       coll = api.addCollection(collection, parent=lib)
       path = api.Var('path', 'block_name + "/"', type='+')
       coll.addVar(path)

# for element in elements:
for element in elements:
    type_ele = element['type']
    name_ele = element['name']
    block_type = api.findBlock(type_ele)
    ele = api.findBlock(name_ele)
    if not ele.isValid():
        ele = api.addElement(name_ele, parent=block_type)
        path = api.Var('path', 'block_name + "/"', type='+')
        ele.addVar(path)
        for dep in deps:
            dep_block = api.addBlock(dep, parent=ele, department=dep.capitalize())
            path = api.Var('path', 'block_name + "/"', type='+')
            dep_block.addVar(path)
            for task in tasks:
                task_block = api.addBlock(task, parent=dep_block, department=dep.capitalize())
                path = api.Var('path', 'block_name + "/"', type='+')
                task_block.addVar(path)
                file_name = api.Var('file_name', '"{}_{}.ma"'.format(element['type'], element['name']), type='+')
                task_block.addVar(file_name)
                task_block.createGraph()
                graph = task_block.openGraph()
                try:
                    graph.loadPreset('init_{}_{}.kgf'.format(dep, task))
                except ValueError:
                    print("Pas de preset pour init_{}_{}.kgf".format(dep, task))

```