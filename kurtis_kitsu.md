### Declaration d'elements
On a vu precedement comment creer a partir de l'api Kurtis les blocks et les 
variables necessaire a la delcaration d'un element de fabrication (toto). Le probleme
c'est qu'il va falloir lancer le script pour chaque nouveau element a creer 
pour le film et devoir le modifier pour correspondre aux nouveau criteres (autre collections, autre nom...).


Habituellement ces elements et ces criteres sont definis dans le prod tracker. Nous
allons utiliser Kitsu et l'objectif sera de faire communiquer Kurtis avec Kitsu
grace a Python et la l'api (application programming interface) *Gazu* de Kitsu.


*presentation de kitsu


### Api Gazu
Grace a Gazu et python nous allons pouvoir manipuler Kitsu et creer des tasks, des assets,
des shots, des reviews... 

Pour le moment nous allons creer des elements dans Kitsu par l'ui et se concentrer
sur leurs recuperation avec python sous forme de dictionnaire.

Il faut commencer par import gazu et ce connecter:
```python
import gazu
gazu.client.set_host("https://eni.cg-wire.com/api")
gazu.log_in("user@mail.com", "default")
```

Il sera utile de recuperer tous les *asset types* de kitsu qui va correspondre a nos
block de *collection* dans la library de notre *Variables Tree*

A la suite du precedent code nous utiliserons:
```python
collections = gazu.asset.all_asset_types() 
```

Attention *Gazu* retourne des dictionnaires contenant beaucoup d'info : 
`[{u'created_at': u'2018-09-03T20:52:24.588856', u'type': u'AssetType', u'updated_at': u'2018-09-03T20:52:24.588864', u'name': u'Chars', u'id': u'342847d0-fac1-4db5-8a5d-041a1d141c3c'}]`
Un dico de la collection chars dans une liste.

Ce qui nous interresse pour Kurtis c'est principalement le son nom, qu'on trouvera
en valeur de la key 'name'.

Passons dans Kurtis et implementons nos precendent script pour generer toutes collections
present dans Kitsu dans Kurtis:
```python 
from kurtis import api
import gazu
gazu.client.set_host("https://eni.cg-wire.com/api")
gazu.log_in("c.laubry@nouvellesimages.xyz", "default") # log to kitsu

dico_types = gazu.asset.all_asset_types() # get list of dico
collections = [d['name'].lower() for d in dico_types] # get name and lower them in a list comprehension

root = api.getRootBlock() # get root block
lib_block = api.findBlock('library') # find block
if not lib_block.isValid():
    lib_block = api.addLibraryBlock('library', parent=root) # create it if doesnt exist
        
for coll in collections:
    coll_block = api.findBlock(coll) # find block
    if not coll_block.isValid():
        coll_block = api.addCollection(coll, parent=lib_block) # create it if doesnt exist
```

A vous d'implementer le reste du code pour finir la creation de nos variables !






