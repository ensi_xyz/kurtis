## Variables Tree
Kurtis est entierement base sur un system de valeurs relatives. 
Toutes ces valeurs sont definies dans la vue `Variables Tree`. Par exemple
le chemin d'enregistrement de mon fichier de modeling, sera genere grace
aux variables presentes dans la `Variables Tree`. 

Pour organiser et structurer nos evaluation de valeurs (ex: notre chemin de modeling) 
il est possible d'utiliser des `Blocks` de variable. Un `Block` permet de grouper
des variables mais aussi de les hierarchiser. Ce qui peut avoir comme avantage de 
concatener (ou soustraire) des variables d'un block parent a un block enfant. 

ex:

    - library *block*
        - path = library/ *variable*
        - props *block*
            - path = library/props *concaten variable* path + block_name
            - toto *block*
                - path = library/props/toto *concaten variable* path + block_name
                - model *block*
                    - path = library/props/toto/model *concaten variable* path + block_name


Dans l'exemple on definit un arbre de variable permettant de structurer le chemin
de fabrication du model toto, ainsi en dupliquant et renomant le block `toto *block*`
il est possible de definir de nouveau element dans props sans modifier les 
valeurs des variables.

## Variables Tree API
Il est possible de creer et arranger les variables depuis l'interface. Mais quand
une centaine d'element doivent etre declare dans la Variable Tree il est plus aise
de passer par l'API Python. 


De maniere general, un nouveau block doit etre cree dans le root, ou dans un 
block deja existant. 

```python
# ceate a new block in root
from kurtis import api
root = api.getRootBlock() # get root item
new_block = api.addBlock('new block', parent=root) # create new block with root as parent argument
```

```pythom
# ceate a new block in an existing block
parent_block = api.findBlock('new block') # get parent block
new_block = api.addBlock('new block', parent=parent_block) # create new block with parent_block as parent argument
```

A ces nouveaux blocks on peut y ajouter des variables.

```python
new_var = api.Var('path', 'block_name+"/"', type='+') # a new Var is defined with name 'path',
# as value parent's block name plus a slash and as concatenate '+' type use to add parent 'path' value. 
new_block.addVar(new_var) # add new_var inside the block.
```

Si un block du meme nom existe deja kurtis retournera une erreur. Ce n'est pas le 
cas des variables vu que le type + permet la concatenation des valeurs.

Exemple de creation d'un arbre a partir d'une `Variables Tree` vide.

```python
from kurtis import api
root = api.getRootBlock()
lib = api.addLibrary('library',parent=root)
path = api.Var('path', 'block_name + "/"', type='+')
lib.addVar(path)

props = api.addCollection('props', parent=lib)
path = api.Var('path', 'block_name + "/"', type='+')
props.addVar(path)

toto = api.addBlock('toto', parent=props)
path = api.Var('path', 'block_name + "/"', type='+')
toto.addVar(path)

mod = api.addBlock('model', parent=toto, department='Modeling')
path = api.Var('path', 'block_name + "/"', type='+')
mod.addVar(path)

work = api.addBlock('work', parent=mod, department='Modeling')
path = api.Var('path', 'block_name + "/"', type='+')
work.addVar(path)

approuved = api.addBlock('approuved', parent=mod, department='Modeling')
path = api.Var('path', 'block_name + "/"', type='+')
approuved.addVar(path)
```

Avec creation de graph preset:
```python
from kurtis import api
root = api.getRootBlock()
lib = api.addLibrary('library',parent=root)
path = api.Var('path', 'block_name + "/"', type='+')
lib.addVar(path)

props = api.addCollection('props', parent=lib)
path = api.Var('path', 'block_name + "/"', type='+')
props.addVar(path)

toto = api.addBlock('toto', parent=props)
path = api.Var('path', 'block_name + "/"', type='+')
toto.addVar(path)

mod = api.addBlock('model', parent=toto, department='Modeling')
path = api.Var('path', 'block_name + "/"', type='+')
mod.addVar(path)

work = api.addBlock('work', parent=mod, department='Modeling')
path = api.Var('path', 'block_name + "/"', type='+')
work.addVar(path)
work.createGraph()
graph = work.openGraph()
graph.loadPreset('init_maya.kgf')

approuved = api.addBlock('approuved', parent=mod, department='Modeling')
path = api.Var('path', 'block_name + "/"', type='+')
approuved.addVar(path)
approuved.createGraph()
graph.loadPreset('init_maya.kgf')

```



Creation des elements a partir de liste et de dico
```python
from kurtis import api

elements = [{'name': 'toto', 'type': 'chars'}, {'name': 'tutu', 'type': 'props'}, {'name': 'tata', 'type': 'sets'}]
collections = ['props', 'chars', 'sets']
deps = ['modeling', 'rigging', 'shading']
tasks = ['work']

# creation du block lib
root = api.getRootBlock()
lib = api.findBlock('library')
if not lib.isValid():
   lib = api.addLibrary('library', parent=root)
   path = api.Var('path', 'block_name + "/"', type='+')
   lib.addVar(path)

# creations des collections
for collection in collections:
    coll = api.findBlock(collection)
    if not coll.isValid():
       coll = api.addCollection(collection, parent=lib)
       path = api.Var('path', 'block_name + "/"', type='+')
       coll.addVar(path)

# for element in elements:
for element in elements:
    type_ele = element['type']
    name_ele = element['name']
    block_type = api.findBlock(type_ele)
    ele = api.findBlock(name_ele)
    if not ele.isValid():
        ele = api.addElement(name_ele, parent=block_type)
        path = api.Var('path', 'block_name + "/"', type='+')
        ele.addVar(path)
        for dep in deps:
            dep_block = api.addBlock(dep, parent=ele, department=dep.capitalize())
            path = api.Var('path', 'block_name + "/"', type='+')
            dep_block.addVar(path)
            for task in tasks:
                task_block = api.addBlock(task, parent=dep_block, department=dep.capitalize())
                path = api.Var('path', 'block_name + "/"', type='+')
                task_block.addVar(path)
                file_name = api.Var('file_name', '"{}_{}.ma"'.format(element['type'], element['name']), type='+')
                task_block.addVar(file_name)
                task_block.createGraph()
                graph = task_block.openGraph()
                try:
                    graph.loadPreset('init_{}_{}.kgf'.format(dep, task))
                except ValueError:
                    print("Pas de preset pour init_{}_{}.kgf".format(dep, task))
```





